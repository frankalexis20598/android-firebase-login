package com.example.frank.testgooglesignin;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginEmailActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "LoginEmailActivity";
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;

    TextInputLayout tilemail, tilpassword;
    Button btnlogin, btnregister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_email);

        // Mapeo de los componentes
        tilemail = (TextInputLayout) findViewById(R.id.til_register_email);
        tilpassword = (TextInputLayout) findViewById(R.id.til_register_pass);
        btnlogin = (Button) findViewById(R.id.btn_email_login);
        btnregister = (Button) findViewById(R.id.btn_email_go_register);

        // Asignamos el evento click
        btnlogin.setOnClickListener(this);
        btnregister.setOnClickListener(this);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    goMainEmailActivity();
                }
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListener);
    }

    private void goMainEmailActivity() {
        Intent intent = new Intent(LoginEmailActivity.this, MainEmailActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_email_login:
                signInFirebaseAccount(
                        tilemail.getEditText().getText().toString(),
                        tilpassword.getEditText().getText().toString()
                );
                break;
            case R.id.btn_email_go_register:
                goRegisterActivity();
                break;
        }
    }

    private void signInFirebaseAccount(String email, String password) {
        if (validateForm()) {
            firebaseAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                goMainEmailActivity();
                            } else {
                                Log.w(TAG, "signInWithEmail:failure", task.getException());
                                Toast.makeText(LoginEmailActivity.this, "La autenticación falló.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = tilemail.getEditText().getText().toString();
        if (TextUtils.isEmpty(email)) {
            tilemail.setErrorEnabled(true);
            tilemail.setError("Requerido.");
            valid = false;
        } else {
            tilemail.setErrorEnabled(false);
            tilemail.setError(null);
        }

        String password = tilpassword.getEditText().getText().toString();
        if (TextUtils.isEmpty(password)) {
            tilpassword.setErrorEnabled(true);
            tilpassword.setError("Requerido.");
            valid = false;
        } else {
            tilpassword.setErrorEnabled(false);
            tilpassword.setError(null);
        }

        return valid;
    }

    private void goRegisterActivity() {
        Intent intent = new Intent(LoginEmailActivity.this, RegisterEmailActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (firebaseAuthListener != null) {
            firebaseAuth.removeAuthStateListener(firebaseAuthListener);
        }
    }
}

package com.example.frank.testgooglesignin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainFacebookActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnlogout;
    ImageView imgfabookuser;
    TextView txtfacebookname, txtfacebookemail, txtfacebookid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_facebook);

        // Mapeo de los componentes
        imgfabookuser = (ImageView) findViewById(R.id.img_facebook_user);
        txtfacebookname = (TextView) findViewById(R.id.txt_facebook_name);
        txtfacebookemail = (TextView) findViewById(R.id.txt_facebook_email);
        txtfacebookid = (TextView) findViewById(R.id.txt_facebook_id);
        btnlogout = (Button) findViewById(R.id.btn_facebook_logout);
        btnlogout.setOnClickListener(this);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null) {
            txtfacebookname.setText(user.getDisplayName());
            txtfacebookemail.setText(user.getEmail());
            txtfacebookid.setText(user.getUid());
            Glide.with(this)
                    .load(user.getPhotoUrl())
                    .into(imgfabookuser);
        } else {
            goLoginFacebookActivity();
        }
    }

    private void goLoginFacebookActivity() {
        Intent intent = new Intent(MainFacebookActivity.this, LoginFacebookActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_facebook_logout:
                facebookLogout();
                break;
        }
    }

    private void facebookLogout() {
        FirebaseAuth.getInstance().signOut();

        LoginManager.getInstance().logOut();
        goLoginFacebookActivity();
    }
}

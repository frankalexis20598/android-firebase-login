package com.example.frank.testgooglesignin;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class RegisterEmailActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "RegisterEmailActivity";
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;

    ProgressBar progressBar;
    ScrollView containerRegister;
    TextInputLayout tilname, tilemail, tilpassword;
    Button btnregister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_email);

        // Mapeo de los componentes
        progressBar = (ProgressBar) findViewById(R.id.progress_bar_register);
        containerRegister = (ScrollView) findViewById(R.id.container_register);
        tilname = (TextInputLayout) findViewById(R.id.til_register_name);
        tilemail = (TextInputLayout) findViewById(R.id.til_register_email);
        tilpassword = (TextInputLayout) findViewById(R.id.til_register_pass);
        btnregister = (Button) findViewById(R.id.btn_email_register);

        // Asignar el evento click
        btnregister.setOnClickListener(this);

        // Configuramos Firebase Auth
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    UserProfileChangeRequest profileUpdate = new UserProfileChangeRequest.Builder()
                            .setDisplayName(tilname.getEditText().getText().toString())
                            .build();
                    user.updateProfile(profileUpdate).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "Perfil de usuario actualizado.");
                            }
                        }
                    });
                    goMainEmailActivity();
                }
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListener);
    }

    private void goMainEmailActivity() {
        Intent intent = new Intent(RegisterEmailActivity.this, MainEmailActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_email_register:
                createFirebaseAccount();
                break;
        }
    }

    private void createFirebaseAccount() {
        progressBar.setVisibility(View.VISIBLE);
        containerRegister.setVisibility(View.GONE);

        String email = tilemail.getEditText().getText().toString();
        String pass = tilpassword.getEditText().getText().toString();

        firebaseAuth.createUserWithEmailAndPassword(email, pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);
                        containerRegister.setVisibility(View.VISIBLE);

                        if (!task.isSuccessful()) {
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(RegisterEmailActivity.this, "La autenticación falló.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (firebaseAuthListener != null) {
            firebaseAuth.removeAuthStateListener(firebaseAuthListener);
        }
    }
}
